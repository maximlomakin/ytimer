# YTimer

### Author: Lomakin Maxim <lom.tik.com@gmail.com>

YTimer is a study service for time tracking. YTimer is developed in Erlang with [Yaws](http://yaws.hyber.org/).
All documentation is in the "docs" directory.

