# API documentation

### Timer object

Attributes| Description
---------:|:--------------------------------
id        | Integer for timer identification.
duration  | Integer. Amount of second that timer has tracked>.
status    | String. Current timer state. Possible values are `active` or `stoped`.

### Create a timer

**Request:** POST `/timer/`
```
{
    "status": "stoped"
}
```
**Response:** 201
```
{
    "id": 2345,
    "duration": 0,
    "status": "stoped"
}
```

## Retrieve a timer

**Request:** GET `/timer/123/`
**Response:** 200
```
{
    "id": 123,
    "duration": 5142356,
    "status": "active"
}
```

### Stop timer

**Request:** PUT `/timer/123/`
```
{
    "status": "stoped"
}
```
**Response:** 200
```
{
    "id": 123,
    "duration": 2143561,
    "status": "stoped"
}
```

### Resume timer

**Request:** PUT `/timer/123/`
```
{
    "status": "active"
}
```
**Response:** 200
```
{
    "id": 123,
    "duration": 1265,
    "status": "active"
}
```

### Reset timer

**Request:** PUT `/timer/123/`
```
{
    "duration": 0
}
```
**Response:** 200
```
{
    "id": 123,
    "duration": 0,
    "status": "stoped"
}
```

