-module(yaws_timer).
-author('maxim@razortheory.com').

-include("/usr/local/lib/yaws/include/yaws_api.hrl").
-compile(export_all).


out (#arg{appmoddata = ""}) ->
    {{Years, Months, Days}, {Hours, Minutes, Seconds}} = calendar:universal_time(),
    [{status, 200},
     {ehtml, [{h2, [], io_lib:format("~4..0b/~2..0b/~2..0b ~2..0b:~2..0b:~2..0b", [Years, Months, Days, Hours, Minutes, Seconds])}]}];

out (_) ->
    [{status, 404},
     {html, "<h2>Not found.</h2><p><i>Yaws timer.</i></p>"}].

